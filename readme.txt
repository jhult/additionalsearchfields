﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              AdditionalSearchFields
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_1_20121022

*******************************************************************************
** Overview
*******************************************************************************

	This component enables additional fields to be added as available fields 
	that can be added to Search Results pages. The field names are defined in 
	a comma-separated list in a preference prompt.
	
	Dynamichtml includes:
	create_core_fields_result_set - Overrides include and calls additional 
	include additional_search_fields
	additional_search_fields - Adds additional fields from the preference 
	prompt, AdditionalSearchFields_FieldNames, to the CoreFields ResultSet
		
	Preference Prompts:
	AdditionalSearchFields_FieldNames - Comma-separated list of additional 
	search field names. Do not include any spaces between commas.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

	build_1_20121022
		- Initial component release